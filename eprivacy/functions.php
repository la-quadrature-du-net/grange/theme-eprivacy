<?php                                                                                                                                                                                                             
# Replace a date in [YYYY-MM-DD] by the number of days between now and then
function replace_date($str)
{
	preg_match('@(?<=\[\[date\|).*(?=\]\])@Uu', $str, $matches);

	$now 	= new DateTime(date('Y-m-d'));
	$then 	= new DateTime($matches[0]);
	$intrv 	= $now->diff($then);

	return preg_replace('@\[\[date\|.*\]\]@Uu', $intrv->format('%a'), $str);
}

# Nav menus on the header of the themes
function register_my_menu() {
    register_nav_menu('header-menu', __( 'Header Menu' ));
    register_nav_menu('front-menu', __('Frontpage Menu'));
}

add_action( 'init', 'register_my_menu');

# Custom Header
$custom_header = array(
    'flex-width' => true,
    'width' => 980,
    'flex-heigth' => true,
    'heigth' => 60,
    'uploads' => True,
);
add_theme_support('custom-header', $custom_header);

# Custom Background
$custom_background = array(
    'default-color' => 'd42620',
);
add_theme_support('custom-background', $custom_background);


# Let's include bootstrap, custom scripts and the like
function enqueue_style_script() {
    wp_enqueue_style('fontface', get_template_directory_uri() . '/css/fontfaces.css');
    wp_enqueue_style('style', get_stylesheet_uri() );
}
add_action('wp_enqueue_scripts', 'enqueue_style_script');

# Custom Title
add_theme_support('title-tag');

function et_widget_areas_init() {
	register_sidebar( array(
		'name'          => __( 'Footer Area #1', 'Serene' ),
		'id'            => 'footer-area-1',
		'before_widget' => '<div id="%1$s" class="f_widget %2$s">',
		'after_widget'  => '</div> <!--.f_widget -->',
		'before_title'  => '<h4 class="widgettitle">',
		'after_title'   => '</h4>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer Area #2', 'Serene' ),
		'id'            => 'footer-area-2',
		'before_widget' => '<div id="%1$s" class="f_widget %2$s">',
		'after_widget'  => '</div> <!--.f_widget -->',
		'before_title'  => '<h4 class="widgettitle">',
		'after_title'   => '</h4>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer Area #3', 'Serene' ),
		'id'            => 'footer-area-3',
		'before_widget' => '<div id="%1$s" class="f_widget %2$s">',
		'after_widget'  => '</div> <!--.f_widget -->',
		'before_title'  => '<h4 class="widgettitle">',
		'after_title'   => '</h4>',
	) );
}
add_action( 'widgets_init', 'et_widget_areas_init' );

?>
