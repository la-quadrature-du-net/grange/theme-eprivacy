<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
    <head>
        <meta charset="<?php bloginfo('charset'); ?>" />

		<title> <?php bloginfo('name'); ?> - <?php bloginfo('description'); ?> </title>

		<link rel="stylesheet" id="style-css"  href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="all" />

        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="pingback" href="<?php bloginfo( 'pinback_url' ); ?>" />
		<link rel="dns-prefetch" href="//s.w.org" />

		<link rel="shortcut icon" href="https://www.laquadrature.net/sites/all/themes/lqdn314/favicon.ico" type="image/x-icon" />

<!--
		<meta property="og:title" content=<?php bloginfo('name'); ?> - <?php bloginfo('description'); ?>>
		<meta property="og:description" content="">
		<meta property="og:image"">
		<meta property="og:url" content="">

		<meta name="twitter:card" content="summary_large_image">
-->

    </head>

    <body>
