<script>

	function opn(that) 
	{
		var div = that.previousSibling

		if (div.style.maxHeight=="0px")
	    {
			div.style.transition = "max-height 1s";
			div.style.maxHeight = screen.height * 2 + "px";
      
			timeoutID = setTimeout(function(){
				div.style.transition = "";
				div.style.maxHeight = "";

				that.className="less";
				that.setAttribute("name",that.innerHTML);
				that.innerHTML="✕";


			}, 200, div);
		}
		else
		{
		    window.scrollTo(0, that.parentNode.offsetTop-80);
			that.className="more";
			that.innerHTML= that.getAttribute("name");
			div.style.maxHeight = "0px";
		}
	}

onload = function() {

	var x;
	var i;
	
	x = document.getElementsByClassName("more");
	for (i = 0; i < x.length; i++)
    	x[i].style.display = "block";

	x = document.getElementsByClassName("hidden");
	for (i = 0; i < x.length; i++)
    	x[i].style.maxHeight = "0"; 
}

</script>

    </body>
</html>

