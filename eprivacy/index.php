<?php                                                                                                                                                                   include('header.php')                                          
		the_post();
		$content = apply_filters( 'the_content', get_the_content() );

	// non-breakable spaces and void lines
		$content = preg_replace('#&nbsp;#', '', $content);
		$content = preg_replace('#<p></p>#', '', $content);
		$content = preg_replace('# ([?!:;»])#u', '&nbsp;\1', $content);
		$content = preg_replace('#« #', '«&nbsp;', $content);

	// header
		$content = preg_replace(
			'#<p><img.*src="(.*)".*/>.*(<h1.*)(?=<p|$)#Us',
			'<header><div id="home" style="background-image:url(\1);"><div id="title">\2</div></div></header><main>',
			$content, 1);

	// intro
		$content = preg_replace(
			'#(<main>)(<p>.*)(?=<p><img)#Us',
			'<section id="intro">\2</section>\1',
			$content, 1);

	// sections
		$content = preg_replace(
			'#<p><img.*src="(.*)".*alt="(.*)".*/></p>.*(<h1).*(>)(.*)(</h1>.*)(?=<p><img|\[\[footer|$)#Us',
			'<section id="\5"><div class="image" style="background-image:url(\1);"><p>\2</p></div><div class="text">\3\4\5\6</div></section>',
			$content);

		$content = preg_replace('#&lt;#', '<', $content);
		$content = preg_replace('#&gt;#', '>', $content);

	// date
		$content = replace_date($content);

	// more
		$content = preg_replace(
			'#<p>\[\[open\|(.*)\]\]</p>(.*)<p>\[\[close.*\]\]</p>#Us',
			'<div class="hidden" style="overflow: hidden;">\2</div><p class="more" style="display:none;" onclick="opn(this);">\1</p>',
			$content);

	// footer
		$content = preg_replace('#\[\[footer\]\].*<h1>(.*)</h1>(.*)$#Us', '</main><footer id="\1"><h1>\1</h1>\2</footer>', $content);

	// menu
		$menu = '<menu><a href="#home">⌂</a>';
		preg_match_all('#(?:class="text">|<footer.*)<h1.*>(.*)</h1>#U', $content, $menus);

		foreach ($menus[1] as $v)
			$menu = $menu . '<a href="#'. $v .'">' . $v . '</a>';

		$menu = $menu . "<span id='lang'><a href='../en'>[en]</a><a href='../fr'>[fr]</a></span>";
		$menu = $menu . '</menu>';
		$content = $menu . $content;

	// end
		echo $content;
	?>

<?php get_footer(); ?>
